import java.util.Scanner;

import javax.naming.InitialContext;

public class Problem18 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int t;
        int n;
        System.out.print("Please select star type [1-4,5 is Exit]: ");
        t = sc.nextInt();

        if (t <= 4) {
            System.out.print("Please input number: ");
            n = sc.nextInt();
            if (t == 1) {
                for (int i = 1; i <= n; i++) {
                    for (int j = 0; j < i; j++) {
                        System.out.print("*");
                    }
                    System.out.println();
                }
            } else if (t == 2) {
                for (int i = 5; i > 0; i--) {
                    for (int j = 0; j < i; j++) {
                        System.out.print("*");
                    }
                    System.out.println();
                }
            } else if (t == 3) {
                for (int i = 0; i < n; i++) {
                    for (int j = 0; j < i; j++) {
                        System.out.print(" ");
                    }
                    for (int j = 0; j < 5 - i; j++) {
                        System.out.print("*");
                    }
                    System.out.println();
                }
            } else if(t == 4) {
                for (int i = n; i >= 0; i--) {
                    for (int j = 0; j < n; j++) {
                        if(j>=i) {
                            System.out.print("*");
                        }else {
                            System.out.print(" ");
                        }
                    }
                    System.out.println();
                }
            }
        } else if (t == 5) {
            System.out.print("Bye bye!!!");
        } else {
            System.out.print("Error: Please input number between 1-5");
        }

    }}
